@extends('layouts.public_zone')

<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div class="content">
        <div class="title m-b-md display-inline">
            {{env('APP_NAME')}}
        </div>

        <div class="links display-inline block-links">
            <a href="#" class="btn btn-primary">
                <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp;Documentation
            </a>
            <a href="#" class="btn btn-primary">
                <i class="fa fa-picture-o" aria-hidden="true"></i></i>&nbsp;Presentation
            </a>
            <a href="#" class="btn btn-primary">
                <i class="fa fa-users" aria-hidden="true"></i>&nbsp;Authors
            </a>
        </div>
    </div>
</div>

